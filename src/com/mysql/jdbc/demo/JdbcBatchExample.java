package com.mysql.jdbc.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class JdbcBatchExample {

	public static void main(String[] args) {
		Connection con= null;
		Statement st = null;
		try {
			// Register the driver class
			Class.forName("com.mysql.cj.jdbc.Driver");
			// create the database connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "admin");
			// set auto commit to false
			con.setAutoCommit(false);
			// create the statement object
			st = con.createStatement();
			// write multiple sqls
			// Execute the SQLs
			String sql1 = "insert into user values (1, 'david', 'gill', 'david@gill.com', 'gill@1234', '1991-10-25', 'M')";
			String sql2 = "insert into user values (2, 'watson', 'light', 'watson@light.com', 'ligh@1234', '1985-11-25', 'M')";
			String sql3 = "insert into user values (3, 'kara', 'joraell', 'kara@jora.com', 'kara@1234', '1987-12-25', 'F')";
			String sql4 = "update user set firstname='supergirl' where id = 3";
			String sql5 = "delete from user where id = 1";
			String sql6 = "insert into user values (4, 'okay', 'java', 'okayjava@youtube.com', 'okay@1234', '1987-12-25', 'M')";
			
			// add sqls to batch by using addBatch()
			st.addBatch(sql1);
			st.addBatch(sql2);
			st.addBatch(sql3);
			st.addBatch(sql4);
			st.addBatch(sql5);
			st.addBatch(sql6);
			
			// Run the batch by calling executeBatch()
			int[] status = st.executeBatch();
			System.out.println("batch execution status = " + Arrays.toString(status));
			// commit the changes to the database 
			con.commit();
			
			// finally close the database connection.
			con.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	}

}
