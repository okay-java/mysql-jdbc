package com.mysql.jdbc.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

public class JdbcBatchExecutionPS {

	public static void main(String[] args) {
		Connection con= null;
		PreparedStatement pst = null;
		try {
			// Register the driver class
			Class.forName("com.mysql.cj.jdbc.Driver");
			// create the database connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/userdb", "root", "admin");
			// set auto commit to false
			con.setAutoCommit(false);
			// Single SQL , set multiple records
			String insertSql = "insert into user values (?, ?, ?, ?, ?, ?,?)";
			// create the prepared statement object
			pst = con.prepareStatement(insertSql);
			// add sqls to batch by using addBatch()
			
			pst.setInt(1, 5);
			pst.setString(2, "Alex");
			pst.setString(3, "Danvers");
			pst.setString(4, "alex@abc.com");
			pst.setString(5, "alex1234");
			pst.setString(6, "1987-12-15");
			pst.setString(7, "F");
			
			pst.addBatch();
			
			pst.setInt(1, 6);
			pst.setString(2, "John");
			pst.setString(3, "John");
			pst.setString(4, "j@jhon.com");
			pst.setString(5, "jhon1234");
			pst.setString(6, "1986-12-15");
			pst.setString(7, "M");
			
			pst.addBatch();
			
			pst.setInt(1, 7);
			pst.setString(2, "Nia");
			pst.setString(3, "nal");
			pst.setString(4, "nia@nal.com");
			pst.setString(5, "nia1234");
			pst.setString(6, "2021-12-15");
			pst.setString(7, "F");
			
			pst.addBatch();
			
			// Run the batch by calling executeBatch()
			 int[] status = pst.executeBatch();
			System.out.println("result of batch execution = " + Arrays.toString(status));
			// commit the changes to the database 
			con.commit();
			// finally close the database connection.
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(pst!=null) {
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	}

}
