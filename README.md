# mysql-jdbc

Java Database connectivity - 5 steps to connect to mysql database
watch the video here https://youtu.be/kb97ga47UPQ

## 5 Steps to connect to the database 

Step -1 : Register the driver 

The forName() method of Class is used to register the driver class (dynamically load the driver class)

Step-2 :  Create the database connection

The getConnection() method of DriverManager class is used to establish connection with the database.

Step-3 : Create the statement or prepared statement object

The createStatement() method of Connection interface is used to create statement..

Step-4 : Execute SQL

The executeQuery()/executeUpdate() method of Statement interface is used to execute queries.

Step-5 : close the database connection

The close() method of Connection interface is used to close the connection.


Download mysql driver

https://dbschema.com/jdbc-driver/MySql.html


Login to mysql database server 

mysql -u root -p


--- create database

create database userdb;

use userdb;


create table user(id int not null auto_increment, 
firstname varchar(50), 
lastname varchar(50), 
email varchar(50), 
password varchar(10), 
birth_date date, 
gender char(1),
primary key(id)); 


show tables;

describe user;

insert into user values (1, 'adam', 'kia', 'adam@kia.com', 'abcd@1234', '2021-12-25', 'M');

insert into user values (2, 'david', 'gill', 'david@gill.com', 'gill@1234', '2021-10-25', 'M');

insert into user values (3, 'watson', 'light', 'watson@light.com', 'ligh@1234', '2021-11-25', 'M');

insert into user values (4, 'kara', 'joraell', 'kara@jora.com', 'kara@1234', '2021-12-25', 'M');

insert into user values (5, 'alex', 'danvers', 'alex@deo.com', 'alex@1234', '2021-12-25', 'M');

Thank you you .. subscribe https://www.youtube.com/okayjava  :) 

